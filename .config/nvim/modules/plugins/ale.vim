let g:ale_fixers = {
      \	'javascript': ['prettier', 'eslint'],
      \ 'typescript': ['prettier', 'eslint'],
\}

let g:ale_linters_ignore = {
                  \ 'javascript': ['tsserver'],
                  \ 'javascript.jsx': ['tsserver'],
                  \}

let g:ale_fix_on_save = 1
let g:ale_history_log_output = 1


" key mappings
nmap <silent> [c <Plug>(ale_previous_wrap)
nmap <silent> ]c <Plug>(ale_next_wrap)
